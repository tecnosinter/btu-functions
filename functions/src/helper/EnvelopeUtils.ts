import Envelope from "../model/beertopup/Envelope";

export default class EnvelopeUtils {

static buildSuccessEntity = async(entity : any) => {
    const envelope = new Envelope();
    envelope.entity = entity;
    envelope.success =  true;
    return envelope;
}
static buildSuccessList = async(list : Array<any>) => {
    const envelope = new Envelope();
    envelope.list = list;
    envelope.success =  true;
    return envelope;
}

static buildError = async(msgList : Array<string>) => {
    const envelope = new Envelope();
    envelope.msg = msgList;
    envelope.success =  false;
    return envelope;
}

}