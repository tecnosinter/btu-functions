
import ResponseList from './ResponseList'
import User from '../../model/commons/User'
import { removeObjectsWithNull } from '../Util'

export default class ResponseEntity<T> {

    entity?: T
    list: ResponseList<T> = new ResponseList<T>()
    user?: User
    msg?: string

    public toJson(): any {
        return removeObjectsWithNull(this)
    }

}
