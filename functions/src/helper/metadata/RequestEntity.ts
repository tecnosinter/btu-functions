import Client from '../../model/commons/Client'
import { auth } from "firebase-admin";

export default class RequestEntity {

    id?: string
    user?: auth.UserRecord
    payload?: any
    client?: Client

}
