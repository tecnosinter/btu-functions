import {isEmpty,isNumber,isNaN, isObject, omitBy, isNil, isArray} from 'lodash'
import Client from '../model/commons/Client';
import BTUConstants from '../model/beertopup/BTUConstants';
import { paramKey } from '../shared/Constant';

const isBlank = (value: any): boolean => {
    return isEmpty(value) && !isNumber(value) || isNaN(value)
}

export const removeObjectsWithNull = (obj: any): any => {
    return (obj).pickBy(isObject)
        .mapValues(removeObjectsWithNull)
        .assign(omitBy(obj, isObject))
        .assign(omitBy(obj, isArray))
        .omitBy(isNil).omitBy(isBlank)
        .value()
}

/**
 * Carrega um campo reference de um documento
 * @param target - documento de onde o campo reference deve ser carregado
 * @param field  - nome do reference a ser carregado
 */
export const loadField = async (target: any, field: string) => {
    if (target[field]) {

        const docRef = await target[field].get();
        if (docRef.exists) {
            target[field] = docRef.data()
            target[field].id = docRef.id
            return { key: target.id, value: target[field], field: field }

        } else {
            return { key: target.id, value: {}, field: field }
        }

    }
    else {
        return { key: target.id, value: {}, field: field }
    }
}

/**
 * Insere os campos nos respectivos objetos
 * @param targets Lista de objetos que irao receber os respectivos campos, deve possuir um campo chamado id
 * @param fields Campos que serao atribuidos aos objetos, cada field deve possuir os campos key, value, field
 */
export const reduceFields = (targets: Array<any>, fields: Array<any>) => {
    targets.forEach((target: any) => {
        const filtereds = fields.filter((field: any) => field.key === target.id)
        filtereds.forEach((targetField: any) => {
            target[targetField.field] = targetField.value
        })
    })
}

export async function buldPathAtClient(client: Client, currentDoc: string) {
    if (client && client.id) {
      return (
        BTUConstants.BTU_CLIENT_DB_PATH.replace(paramKey.client,client.id) + currentDoc
      );
    } else {
      throw new Error("Client is required for doc path building");
    }
  }

  export async function buildPathAtBranch(client: Client, currentDoc: string) {
    if ((client && client.id) && client.branch) {
      return (
        BTUConstants.BRANCH_PATH.replace(paramKey.client,client.id).replace(paramKey.branch,client.branch) + currentDoc
      );
    } else {
      throw new Error("Client and branch are required for doc path building");
    }
  }
