import { firestore } from "../../../shared/Config";
import Sale from '../../../model/beertopup/Sale';
import EnvelopeUtils from '../../../helper/EnvelopeUtils';
import { validationResult } from 'express-validator/check'
import { buildPathAtBranch } from "../../../helper/Util";
import { Timestamp } from "@google-cloud/firestore";


const collectionName = 'Sale';

export const list = async (req, res,next) => {
  try {
    const docPath = await buildPathAtBranch(req.requestEntity.client,collectionName);
    const rows = await firestore.collection(docPath).get();
    const result = Array<Sale>();
    for (const doc of rows.docs) {
      const sale = new Sale();
      sale.id =  doc.id;
      sale.description = doc.data().description;
      sale.targets = doc.data().saleTarget;
      sale.startAt = doc.data().startAt;
      sale.endAt = doc.data().endAt;
      sale.imageUrl = doc.data().imageUrl;
      sale.goal= doc.data().imageUrl;   
      result.push(sale);
    }
    res.status(200).json(await EnvelopeUtils.buildSuccessList(result)).end();
  } catch (err) {
    console.error(err);
    res.status(500).json(await EnvelopeUtils.buildError([`Error : ${err}`])).end();
  }
};

export const findById = async (req, res,next) => {
  try {  
    const docPath = await buildPathAtBranch(req.requestEntity.client,collectionName);
    const doc = await firestore.collection(docPath).doc(req.params.id).get();
    if (doc.exists && doc.data()) {
      const sale = new Sale();
      const objData = doc.data();
      sale.id =  doc.id;
      sale.description =  objData ? objData.description : '';
      sale.targets = objData ? objData.saleTarget : [];
      sale.startAt = objData ? objData.startAt : null;
      sale.endAt = objData ? objData.endAt : null;
      sale.imageUrl = objData ? objData.imageUrl : '';
      sale.goal= objData ? objData.imageUrl : null;   
        
      res.status(200).json(await EnvelopeUtils.buildSuccessEntity(sale)).end();
    } else {
      res.status(200).json(await EnvelopeUtils.buildError(['Registro não localizado.'])).end();
    }
  } catch (err) {
    console.error(err);
    res.status(500).json(await EnvelopeUtils.buildError([`Error: ${err}`])).end();
  }
};

export const create = async (req, res,next) => {
const errors = await validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json(await EnvelopeUtils.buildError([`Error: ${errors}`]));
  }
  try {
    const docPath = await buildPathAtBranch(req.requestEntity.client,collectionName);
      const documentReference = await firestore.collection(docPath).add({       
        description: req.body.description,
        targets: req.body.targets,
        startAt: Timestamp.fromMillis(req.body.startAt),
        endAt: Timestamp.fromMillis(req.body.endAt),
        imageUrl: req.body.imageUrl,
        goal: req.body.goal,

      });
      res.status(201).json(await EnvelopeUtils.buildSuccessEntity({"id":documentReference.id})).end();
  } catch (err) {
      console.error(err);
      res.status(500).json(await EnvelopeUtils.buildError([`Error: ${err}`])).end();
  }
};

export const remove = async (req, res, next) => {

  try {
    const docPath = await buildPathAtBranch(req.requestEntity.client,collectionName);
      await firestore.collection(docPath).doc(req.params.id).delete();
      res.status(201).json(await EnvelopeUtils.buildError(['Registro excluído com sucesso.'])).end();
  } catch (err) {
      console.error(err);
      res.status(500).json(await EnvelopeUtils.buildError([`Error: ${err}`])).end();
  }
};

export const update = async (req, res, next) => {

  try {
    const docPath = await buildPathAtBranch(req.requestEntity.client,collectionName);
      await firestore.collection(docPath).doc(req.body.id).set({
        description: req.body.description,
        targets: req.body.targets,
        startAt: Timestamp.fromMillis(req.body.startAt),
        endAt: Timestamp.fromMillis(req.body.endAt),
        imageUrl: req.body.imageUrl,
        goal: req.body.goal,
      }, {
          merge: true
      });
      res.status(201).json(await EnvelopeUtils.buildError(['Registro atualizado com sucesso.'])).end();
  } catch (err) {
      console.error(err);
      res.status(500).json(await EnvelopeUtils.buildError([`Error: ${err}`])).end();
  }
};

export const rate = async (req, res, next) => {

  try {
    const docPath = await buildPathAtBranch(req.requestEntity.client,collectionName);
      await firestore.collection(docPath).doc(req.body.id).set({
        rate: req.body.rate,
      }, {
          merge: true
      });
      res.status(201).json(await EnvelopeUtils.buildError(['Avaliação registrada com sucesso.'])).end();
  } catch (err) {
      console.error(err);
      res.status(500).json(await EnvelopeUtils.buildError([`Error: ${err}`])).end();
  }
};
