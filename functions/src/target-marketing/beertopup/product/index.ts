import { firestore } from "../../../shared/Config";
import {DocumentData} from '@google-cloud/firestore'
import Product from "../../../model/beertopup/Product";
import EnvelopeUtils from "../../../helper/EnvelopeUtils";
import { validationResult } from "express-validator/check";
import  BTUConstants from '../../../model/beertopup/BTUConstants'
import Package from "../../../model/beertopup/Package";
import { buldPathAtClient, loadField } from "../../../helper/Util";

const COLLECTION = 'Product';

export const list = async (req, res, next) => {
  try {
    
    const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
    
    const querySnapshot = await firestore.collection(basePath).get();

    const result = Array<Product>();

    for (const doc of querySnapshot.docs) {
      const product = new Product();  
      await product.buildLazy(doc);
      await loadField(doc,'package');    
      result.push(product);
    }
    res.status(200).json(await EnvelopeUtils.buildSuccessList(result)).end();
  } catch (err) {
    console.error(err);
    res.status(500).json(await EnvelopeUtils.buildError([`Error : ${err}`])).end();
  }
};

export const findById = async (req, res, next) => {
  try {
    const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
    const doc = await firestore.collection(basePath).doc(req.params.id).get();
    const productEntity = new Product();
    if (doc.exists && doc.data()) {
      const objData = doc.data();
      productEntity.id = doc.id;
      productEntity.description = objData ? objData.description : "";
      productEntity.price = objData ? objData.price : "";
      productEntity.name = objData ? objData.price : "";
      const p = objData ? objData.package : null;
      const pId = p.id;
      const pp = await firestore.doc(BTUConstants.BTU_BASE_DB_PATH + '/ProductPackage/' + pId).get();
      
      if(pp.exists){
        const ppp = new Package();
        const data = pp.data();
        ppp.capacity = data ? data.capacity : null;
        ppp.description = data ? data.description : null;
        productEntity.package = ppp;
      }    
      res.status(200).json(await EnvelopeUtils.buildSuccessEntity(productEntity)).end();
    } else {
      res.status(200).json(await EnvelopeUtils.buildError(["Registro não localizado."])).end();
    }
  } catch (err) {
    console.error(err);
    res.status(500).json(await EnvelopeUtils.buildError([`Error: ${err}`])).end();
  }
};

export const create = async (req, res, next) => {
  const errors = await validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json(await EnvelopeUtils.buildError([`Error: ${errors}`]));
  }
  try {
    const payload = req.requestEntity.payload;
    const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
    
    const newDoc = await firestore.collection(basePath).add({
      description: payload.description,
      name: payload.name,
      price: payload.price,
      package: firestore.doc(BTUConstants.COLLECTION + '/' + BTUConstants.DOC + '/ProductPackage/' + payload.package),
      type: firestore.doc(BTUConstants.COLLECTION + '/' + BTUConstants.DOC + '/DataType/Product/ProductType/' + payload.type),
    });
    res.status(201).json(await EnvelopeUtils.buildSuccessEntity({ id: newDoc.id })).end();
  } catch (err) {
    console.error(err);
    res.status(500).json(await EnvelopeUtils.buildError([`Error: ${err}`])).end();
  }
};

export const remove = async (req, res, next) => {
  try {
    const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
    await firestore.collection(basePath).doc(req.params.id).delete();
    res.status(201).json(await EnvelopeUtils.buildError(["Registro excluído com sucesso."])).end();
  } catch (err) {
    console.error(err);
    res.status(500).json(await EnvelopeUtils.buildError([`Error: ${err}`])).end();
  }
};

export const update = async (req, res, next) => {
  try {
    const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
    await firestore.collection(basePath).doc(req.body.id).set(
        {
          description: req.body.description,
          price: req.body.price,
          name: req.body.name,
          package: firestore.doc(BTUConstants.COLLECTION + '/' + BTUConstants.DOC + '/ProductPackage/' + req.body.package),
          type: firestore.doc(BTUConstants.COLLECTION + '/' + BTUConstants.DOC + '/DataType/Product/ProductType/' + req.body.type),
        },
        {
          merge: true
        }
      );
    res.status(201).json(await EnvelopeUtils.buildError(["Registro atualizado com sucesso."])).end();
  } catch (err) {
    console.error(err);
    res.status(500).json(await EnvelopeUtils.buildError([`Error: ${err}`])).end();
  }
};

export const search = async (req, res, next) => {
  const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
  const producRef = await firestore.collection(basePath);
  const queryRef = producRef.where("description", "==", req.params.description);
  const queryResult = await queryRef.get();
  const result = Array<DocumentData>();
  if (queryResult.empty) {
    res.status(200).json(await EnvelopeUtils.buildError(["Registro não localizado."])).end();
  } else {
    for (const doc of queryResult.docs) {
      const objData = doc.data();
      const productEntity = new Product();

      productEntity.id = doc.id;
      productEntity.description = objData ? objData.description : "";
      productEntity.price = objData ? objData.price : "";
      result.push(objData);
    }
    res.status(200).json(await EnvelopeUtils.buildSuccessList(result)).end();

  }
};
