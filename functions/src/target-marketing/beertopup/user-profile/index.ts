import { firestore } from "../../../shared/Config";
// import {DocumentData} from '@google-cloud/firestore'

import EnvelopeUtils from "../../../helper/EnvelopeUtils";
import { validationResult } from "express-validator/check";


import { buldPathAtClient } from "../../../helper/Util";


const COLLECTION = 'UserProfile';



export const create = async (req, res, next) => {
  
    const errors = await validationResult(req);
  
  if (!errors.isEmpty()) {
    return res.status(422).json(await EnvelopeUtils.buildError([`Error: ${errors}`]));
  }

  if(!req.requestEntity.payload){
    return res.status(500).json(await EnvelopeUtils.buildError(['Profile info not found']));
  }

  try {  
    const newDoc = await firestore.collection(COLLECTION).add(req.requestEntity.payload);
    res.status(201).json(await EnvelopeUtils.buildSuccessEntity({ id: newDoc.id })).end();
  } catch (err) {
    console.error(err);
    res.status(500).json(await EnvelopeUtils.buildError([`Error: ${err}`])).end();
  }
};

export const remove = async (req, res, next) => {
  try {
    const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
    await firestore.collection(basePath).doc(req.params.id).delete();
    res.status(201).json(await EnvelopeUtils.buildError(["Registro excluído com sucesso."])).end();
  } catch (err) {
    console.error(err);
    res.status(500).json(await EnvelopeUtils.buildError([`Error: ${err}`])).end();
  }
};

export const update = async (req, res, next) => {
  try {
    const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
    await firestore.collection(basePath).doc(req.body.id).set(req.requestEntity.payload,{merge: true});
    res.status(201).json(await EnvelopeUtils.buildError(["Registro atualizado com sucesso."])).end();
  } catch (err) {
    console.error(err);
    res.status(500).json(await EnvelopeUtils.buildError([`Error: ${err}`])).end();
  }
};