import { firestore } from "../../../shared/Config";
import  Order from "../../../model/beertopup/Order";
import EnvelopeUtils from "../../../helper/EnvelopeUtils";
import { validationResult } from "express-validator/check";
import OrderItem from "../../../model/beertopup/OrderItem";
import Product from "../../../model/beertopup/Product";
import { buldPathAtClient, buildPathAtBranch } from "../../../helper/Util";
import Client from "../../../model/commons/Client";


const COLLECTION = "Event";

export const list = async (req, res, next) => {
    try {
  
      const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
  
      const rows = await firestore.collection(basePath).get();
      const result = Array<Order>();
      for (const doc of rows.docs) {
        const order = new Order();
        order.id = doc.id;
        order.time = doc.data().time;
        order.itens = doc.data().itens;
        order.total = doc.data().total;
  
        result.push(order);
      }
      res
        .status(200)
        .json(await EnvelopeUtils.buildSuccessList(result))
        .end();
    } catch (err) {
      console.error(err);
      res
        .status(500)
        .json(await EnvelopeUtils.buildError([`Error : ${err}`]))
        .end();
    }
  };
  
  export const findById = async (req, res, next) => {
    try {
  
      const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
  
      const doc = await firestore.collection(basePath).doc(req.params.id).get();
  
      if (doc.exists && doc.data()) {
        const order = new Order();
        const objData = doc.data();
        order.id = doc.id;
        order.time = objData ? objData.time : "";
        order.itens = objData ? objData.itens : [];
        order.total = objData ? objData.startAt : null;
  
        res
          .status(200)
          .json(await EnvelopeUtils.buildSuccessEntity(order))
          .end();
      } else {
        res
          .status(200)
          .json(await EnvelopeUtils.buildError(["Registro não localizado."]))
          .end();
      }
    } catch (err) {
      console.error(err);
      res
        .status(500)
        .json(await EnvelopeUtils.buildError([`Error: ${err}`]))
        .end();
    }
  };
  
  export const remove = async (req, res, next) => {
    try {
      const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
      await firestore.collection(basePath).doc(req.params.id).delete();
      res
        .status(201)
        .json(await EnvelopeUtils.buildError(["Registro excluído com sucesso."]))
        .end();
    } catch (err) {
      console.error(err);
      res
        .status(500)
        .json(await EnvelopeUtils.buildError([`Error: ${err}`]))
        .end();
    }
  };
  
  export const update = async (req, res, next) => {
    try {
      const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
      await firestore.collection(basePath).doc(req.body.id)
        .set(
          {
            description: req.body.description,
            targets: req.body.targets,
            startAt: req.body.startAt,
            endAt: req.body.endAt,
            imageUrl: req.body.imageUrl,
            goal: req.body.imageUrl
          },
          {
            merge: true
          }
        );
      res
        .status(201)
        .json(
          await EnvelopeUtils.buildError(["Registro atualizado com sucesso."])
        )
        .end();
    } catch (err) {
      console.error(err);
      res
        .status(500)
        .json(await EnvelopeUtils.buildError([`Error: ${err}`]))
        .end();
    }
  };
  
  export const rate = async (req, res, next) => {
    try {
      const basePath = await buldPathAtClient(req.requestEntity.client,COLLECTION);
      await firestore
        .collection(basePath).doc(req.body.id).set(
          {
            rate: req.body.rate
          },
          {
            merge: true
          }
        );
      res
        .status(201)
        .json(
          await EnvelopeUtils.buildError(["Avaliação registrada com sucesso."])
        )
        .end();
    } catch (err) {
      console.error(err);
      res
        .status(500)
        .json(await EnvelopeUtils.buildError([`Error: ${err}`]))
        .end();
    }
  };
  
  export const create = async (req, res, next) => {
    const errors = await validationResult(req);
  
    if (!errors.isEmpty()) {
      return res
        .status(422)
        .json(await EnvelopeUtils.buildError([`Error: ${errors}`]));
    }
  
    try {
      const { itens, orderTotal } = await calculateOrderValue(req.body.itens);
      const basePath = await buildPathAtBranch(req.requestEntity.client,COLLECTION);
      const documentReference = await firestore.collection(basePath).add({
        time: new Date(),
        itens: itens,
        total: orderTotal
      });
      res
        .status(201)
        .json(
          await EnvelopeUtils.buildSuccessEntity({ id: documentReference.id })
        )
        .end();
    } catch (err) {
      console.error(err);
      res
        .status(500)
        .json(await EnvelopeUtils.buildError([`Error: ${err}`]))
        .end();
    }
  };
  
  async function calculateOrderValue(itens: Array<OrderItem>) {
    const productIdList = itens.map((item: OrderItem) => {
      return item.product.id;
    });
  
    const producList = Array<Product>();
    const client  = new Client();
    client.id = 'Bebs';
    //TODO - Adicionar request- remover hardcode
    const basePath = await buldPathAtClient(client,'Product');
  
    for (const id of productIdList) {
      const doc = await firestore.collection(basePath).doc(id).get();
  
      if (doc.exists && doc.data()) {
        const product = new Product();
        const objData = doc.data();
        product.id = doc.id;
        product.price = objData ? objData.price : "";
        producList.push(product);
      }
    }
  
    for (const orderItem of itens) {
      const fullProduct = producList.filter(p => {
        return (p.id = orderItem.product.id);
      });
      orderItem.product.price = fullProduct[0].price;
    }
  
    const orderTotal = itens.reduce((acc, p: OrderItem) => {
      return p.product.price * p.quantity;
    }, 0);
    return { itens, orderTotal };
  }