export const paramKey = {
    client: '{CLIENT}',
    branch: '{BRANCH}'
}
export const targetMarketing = {
    beerTopUp: 'BeerTopUp',
    pluralCard: 'PluralCard',
    codama: 'Codama'
}
export const collection = {
    product:`TargetMarketing/${targetMarketing.beerTopUp}/Client/${paramKey.client}/Product`,
    event:`TargetMarketing/${targetMarketing.beerTopUp}/Client/${paramKey.client}/Event`,
    user: 'User',
    security: 'Security',
    function: 'Function',
    locale: 'User',
    dataType: 'TargetMarketing/BeerTopUp/DataType',
    productPackage: 'TargetMarketing/BeerTopUp/ProductPackage',
    productType: 'ProductType',
    eventType: 'EventType',
    measure: 'MeasureType'
}
export const document = {
    product: 'Product',
    permission: 'Permission',
    locales: {
        beerTopUp: 'BEER_TOP_UP',
        branchOffice: 'BRANCH_OFFICE',
        targetClient: 'TARGET_CLIENT'
    },
    functions: {
        consumer: 'CONSUMER',
        owner: 'OWNER'
    }
}
export const header = {
    client: 'x-codama-client',
    branch: 'x-btu-branch',
    targetMarketing: 'x-codama-client'
}
export const fieldName = {
    product: {
        target: 'product',
        package : 'package',
        type: 'type',
        price: 'price'
    },
    event: {
        target: 'event',
        description : 'description',
        startAt: 'startAt',
        endAt: 'endAt',
        type: 'type'
    }
}