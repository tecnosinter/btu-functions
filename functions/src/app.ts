import express from "express"
import cors from "cors"
import productRoute from './routes/ProductRoute'
import saleRoute from './routes/saleRoute'
import orderRoute from './routes/OrderRoute'
import {isUserAuthenticated}  from './middleware/AuthMiddleware'

const products = express()

products.use(cors());
products.disable("x-powered-by");
products.use(isUserAuthenticated);
products.use('/',productRoute);

const orders = express();
orders.disable("x-powered-by");
orders.use(isUserAuthenticated);
orders.use('/',orderRoute);

const sales = express();
sales.disable("x-powered-by");
sales.use(isUserAuthenticated);
sales.use('/',saleRoute);

console.log(process.env.FUNCTION_NAME); 

export {
    products,
    orders,
    sales
} 
