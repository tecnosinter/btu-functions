import * as express from 'express'
import { body } from 'express-validator/check'
import * as productService from '../target-marketing/beertopup/product'

const productRouter = express.Router();

productRouter.get('',productService.list);

productRouter.get('/:id',productService.findById);

productRouter.get('/search/:description',productService.search);

productRouter.post('',body('description').trim(), productService.create);

productRouter.delete('/:id',productService.remove);

productRouter.put('',productService.update);

export = productRouter;