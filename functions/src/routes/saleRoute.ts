import * as express from 'express'
import { body } from 'express-validator/check'
import * as saleService from '../target-marketing/beertopup/sale'

const saleRouter = express.Router();

saleRouter.get('',saleService.list);

saleRouter.get('/:id',saleService.findById);

saleRouter.post('',body('description').trim(), saleService.create);

saleRouter.delete('/:id',saleService.remove);

saleRouter.put('',saleService.update);

saleRouter.put('/rate',saleService.rate);

export = saleRouter;