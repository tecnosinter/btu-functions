import * as express from 'express'
import { body } from 'express-validator/check'
import * as orderService from '../target-marketing/beertopup/order'

const orderRouter = express.Router();

orderRouter.get('',orderService.list);

orderRouter.get('/:id',orderService.findById);

orderRouter.post('',body('description').trim(), orderService.create);

orderRouter.delete('/:id',orderService.remove);

orderRouter.put('',orderService.update);

export = orderRouter;