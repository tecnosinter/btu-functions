import * as express from 'express'
import * as systemService from '../system/auth'

const systemRouter = express.Router();

systemRouter.post('', systemService.generateToken);

export = systemRouter;