import { DocumentType } from '../beertopup/DocumentType'
import { AuthProvider } from './AuthProvider'
import Address from './Address';
import BillingInfo from './BillingInfo';

export default class UserProfile {

    id? : string;
    firstname: string;
    lastname: string;
    cellphone: string;
    document: string;
    documentType: DocumentType;
    auhtProvider: AuthProvider;
    emailVerified: false;   
    imageurl: string;
    birthday: Date;
    gender: string;
    maritalStatus: string; 
    educationLevel: string;
    address: Address;  
    billingInfo: BillingInfo;
}