export default class Address {

    id?: string;
    zipCode: number;
    addressLine1: string;
    addressLine2: string;
    number: number;
    neighbourhood: string;
    city: string;
    state: string;
}