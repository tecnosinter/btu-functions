export default class Client  {

    id? : string
    name?: string
    activityDescription?: string
    branch: string

    public buildLazy(json: any): void {
       this.name = json.name
       this.activityDescription = json.activityDescription
    }

    public buildEager(json: any): void {
       this.name = json.name
       this.activityDescription = json.activityDescription
    }

}