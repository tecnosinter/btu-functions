import { PaymentMethod } from '../pluralcard/PaymentMethod'
import Address from './Address';
export default class BillingInfo  {
    id: string;
    paymentMethod: PaymentMethod;
    cardNumber: number;
    expirationMonth: number;
    expirationYear: number;
    firtsName: string;
    lastName: string;
    billingAddres: Address;

}