export default class Event  {
    id: string;
    description: string;
    imageUrl: string;
    performers: Array<string>;
    startAt: Date;
    endAt: Date;
    rate: number;
}