import {ProductType} from './ProductType'
import Package from './Package'
import { DocumentData } from '@google-cloud/firestore';

export default class Product  {
    id: string;
    description: string;
    price: number;
    name: string;
    type: ProductType;
    package: Package;
    rate: number;
    points: number;
  
    public async buildEager(qds: DocumentData): Promise<void> {
        this.id = qds.id
        this.name = qds.data().name
        this.description = qds.data().description
        this.price = qds.data().price
    }

    public async buildLazy(qds: DocumentData): Promise<void> {
        this.id = qds.id
        this.name = qds.data().name
        this.description = qds.data().description
        this.price = qds.data().price
     
    }
}