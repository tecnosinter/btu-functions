import OrderItem from './OrderItem'

export default class Order  {
    id: string;
    itens: Array<OrderItem>;
    time: Date;
    total: number;
  
}