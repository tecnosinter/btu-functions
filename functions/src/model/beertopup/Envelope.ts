export default class Envelope  {
    entity: any;
    list: Array<any>;
    success: boolean;
    msg: Array<string>;
    pagination:{
        page: number,
        total: number
    }
    
   
}