import { paramKey } from "../../shared/Constant";

export default class BTUConstants {
    static readonly COLLECTION = 'TargetMarketing';
    static readonly DOC = 'BeerTopUp';
    static readonly BTU_BASE_DB_PATH = '/TargetMarketing/BeerTopUp/'
    static readonly BTU_CLIENT_DB_PATH = `/TargetMarketing/BeerTopUp/Client/${paramKey.client}/`
    static readonly CRED = 'C:/Development/google/serverless/beer-61d77-da61ae105b85.json';
    static readonly BRANCH_PATH = `/TargetMarketing/BeerTopUp/Client/${paramKey.client}/BranchOffice/${paramKey.branch}/`
}