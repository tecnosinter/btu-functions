import Product from "./Product";
import Event from "./Event";

export default class SaleItem  {
    id: string;
    description: string;  
    product: Product;
    concert: Event;
    goal: number;
  
}