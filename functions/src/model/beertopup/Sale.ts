import SaleItem from './SaleItem'

export default class Sale  {
    id: string;
    description: string;
    targets: Array<SaleItem>;
    startAt: Date;
    endAt: Date; 
    imageUrl: string;
    goal: number;
    rate: number;
}