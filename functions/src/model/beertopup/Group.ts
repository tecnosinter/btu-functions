import UserGroup  from "./UserGroup";

export default class Group  {
    id: string; 
    users: Array<UserGroup>;
    establishAt: Date;
    totalPoints: number;   
    active: boolean;
    goal: number;

}