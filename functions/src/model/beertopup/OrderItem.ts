import Product from './Product'

export default class OrderItem  {
    id: string;
    product: Product;
    quantity: number;
    total: number;
 
}