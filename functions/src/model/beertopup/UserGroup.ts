import  User  from "../commons/User";

export default class UserGroup  {
    id: string; 
    user: User;
    joinAt: Date;
    totalPoints: number;
    responsable: boolean;
    active: boolean;
    goal: number;
    productPointRatio: number;  
}