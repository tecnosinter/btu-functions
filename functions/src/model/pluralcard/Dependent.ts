
import { DocumentType } from '../beertopup/DocumentType'
import { AuthProvider } from '../commons/AuthProvider'
import User from '../commons/User'
import { DependentRelation } from './DependentRelation';

export default class Dependent  {
    id: string;
    username: string;
    firstname: string;
    lastname: string;
    email: string;
    phone: string;
    document: string;
    documentType: DocumentType;
    auhtProvider: AuthProvider;
    emailVerified: false;
    address: string;
    imageurl: string;
    birthDate: Date;  
    responsable: User; 
    responsableRelation: DependentRelation;

}