export default class PaymentHistory  {
    id: string;  
    paymentDate: Date;
    paymentDue:Date;
    value: number; 
}