import * as admin from "firebase-admin";
import RequestEntity from "../helper/metadata/RequestEntity";
import Client from "../model/commons/Client";
import { header } from "../shared/Constant";

export const isUserAuthenticated = async (req: any, res: any, next: any) => {
  const authHeader =
    req.headers["Authorization"] || req.headers["authorization"];

  if (authHeader) {
    try {
      const token = getBearerToken(authHeader);

      const decodedToken = await verifyTokenAndGetUID(token);   

      const requestEntity = new RequestEntity();

      if (req.headers[header.client]) {
        requestEntity.client = new Client();

        requestEntity.client.id = req.headers[header.client];
        requestEntity.client.branch = req.headers[header.branch];

        if (req.method === "POST" || req.method === "PUT" || req.method === "PATCH") {
          requestEntity.payload = req.body;
        }
        requestEntity.user = await admin.auth().getUser(decodedToken.uid);    
        
        req.requestEntity = requestEntity;

        next();
      }else {
        console.error("AUTHORIZATION NOT DEFINED");
        res.status(403).json({ status: 403, message: "FORBIDDEN" });
      }
    } catch (error) {
      console.error("ERRO AO RECUPERAR TOKEN", error);
      res.status(403).json({ message: "FORBIDDEN" });
      next(error);
    }
  } else {
    console.error("AUTHORIZATION NOT DEFINED");
    res.status(403).json({ status: 403, message: "FORBIDDEN" });
  }
};

function getBearerToken(authorization: string): string {
  if (authorization) {
    const match = authorization.match(/^Bearer\s+([^\s]+)$/);
    if (match) {
      return match[1];
    } else {
      throw new Error("malformed header authorization");
    }
  } else {
    throw new Error("header not defined");
  }
}

async function verifyTokenAndGetUID(token: string) {
  try {
    return admin.auth().verifyIdToken(token);
  } catch (error) {
    throw error;
  }
}
