import * as admin from 'firebase-admin'

export const generateToken = async (req, res, next) => {

    try {
        const client = req.requesEntity.client;
        const user = req.requesEntity.user;
        if (client && user) {
            const additionalClaims = {
                targetMarketing: client.id,
                client: client.uid
            }
    
            return await admin.auth().createCustomToken(user.uid, additionalClaims);
        }else{
            throw new Error('UNAUTHORIZED');         
        }

    } catch (error) {
        throw error;
    }
 }